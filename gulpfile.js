var gulpfile = require('gulp');
var sass = require('gulp-sass');
var replace = require('gulp-replace');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');

gulpfile.task('styles_sass', function() {
	return gulpfile.src('./sass/styles.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('styles.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});
gulpfile.task('home', function() {
	return gulpfile.src('./sass/style_home.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_home.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});
gulpfile.task('collection', function() {
	return gulpfile.src('./sass/style_collection.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_collection.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});
gulpfile.task('product', function() {
	return gulpfile.src('./sass/style_product.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_product.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});

gulpfile.task('promo_man_woman', function() {
	return gulpfile.src('./sass/style_promo_man_woman.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_promo_man_woman.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});
gulpfile.task('blog', function() {
	return gulpfile.src('./sass/style_blog.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_blog.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});
gulpfile.task('cart', function() {
	return gulpfile.src('./sass/style_cart.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_cart.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});
gulpfile.task('passwords', function() {
	return gulpfile.src('./sass/style_passwords.scss')
		.pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(rename('style_passwords.scss.liquid'))
		.pipe(replace('"{{', '{{'))
		.pipe(replace('}}"', '}}'))
		.pipe(gulpfile.dest('./assets/'));
});

gulpfile.task('theme_js', function() {
	return gulpfile.src(['./assets/theme.js'])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulpfile.dest('./assets/'))
});

gulpfile.task('jquery_bez_js', function() {
	return gulpfile.src(['./assets/jquery.bez.js'])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulpfile.dest('./assets/'))
});

gulpfile.task('header_menu_desktop_js', function() {
	return gulpfile.src(['./assets/header-menu-desktop.js'])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulpfile.dest('./assets/'))
});

gulpfile.task('header_menu_mobile_js', function() {
	return gulpfile.src(['./assets/header-menu-mobile.js'])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulpfile.dest('./assets/'))
});

gulpfile.task('mgutil_js', function() {
	return gulpfile.src(['./assets/mgutil.js'])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulpfile.dest('./assets/'))
});

gulpfile.task('default', function() {
	gulpfile.watch(['./sass/**/*.scss'], gulpfile.series('styles_sass', 'home', 'collection', 'product', 'promo_man_woman', 'blog', 'passwords', 'cart'));
	gulpfile.watch(['./assets/mgutil.js'], gulpfile.series('mgutil_js'));
	gulpfile.watch(['./assets/theme.js'], gulpfile.series('theme_js'));
	gulpfile.watch(['./assets/jquery.bez.js'], gulpfile.series('jquery_bez_js'));
});
