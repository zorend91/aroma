function get_discount() {
    jQuery.getJSON('/cart.js', function(cart) {
        // now have access to Shopify cart object
        var cart_items = {};
        for (var i= 0; i < cart.items.length; i++){
            cart_items[cart.items[i].product_id] = cart.items[i].quantity;
        }
        var new_user = localStorage.getItem('new_user');
        if (new_user == undefined){
            new_user = 0;
        }
        jQuery.ajax({
            type: 'POST',
            url : 'https://p2.tns.company/kit_offer.php',
            dataType: 'json',
            data : {
                'kit_offer': 1,
                'cart_items': cart_items,
                'new_user': new_user,
                // 'new_user': 1,
                'items_subtotal_price': cart.items_subtotal_price
            },
            success: function(response){
                var previousDiscount = localStorage.getItem('discount_id');
                var previousRule = localStorage.getItem('rule_id');
                var previousDiscountName = localStorage.getItem('discount_code');
                var discount_value = localStorage.getItem('discount_value');
                if (previousDiscount && previousDiscount != undefined){
                    jQuery.ajax({
                        type: 'POST',
                        url : 'https://p2.tns.company/kit_offer.php',
                        data : {
                            'remove_discount_code': 1,
                            'discount_code': previousDiscount,
                            'price_rule': previousRule,
                            'discount_name': previousDiscountName
                        },
                        success: function(){
                            console.log('removed');
                            localStorage.removeItem('discount_id');
                            localStorage.removeItem('rule_id');
                            localStorage.removeItem('discount_code_end');
                            localStorage.removeItem('discount_code');
                            localStorage.removeItem('discount_value');
                            localStorage.setItem('discount_id',response.discount_name);
                            localStorage.setItem('rule_id',response.price_rule);
                            localStorage.setItem('discount_code',response.discount_code);
                            localStorage.setItem('discount_value', response.discount_value);
                            var ts = Math.round(new Date().getTime() / 1000);
                            var tsTomorrow = ts + (24 * 3600);
                            localStorage.setItem('discount_code_end',tsTomorrow);
                            jQuery.ajax({
                                url: "https://aromahigh.com/discount/"+response.discount_code
                            });

                            jQuery.getJSON('/cart.js', function(cart_end) {
                                console.log(cart_end);
                                var total = cart_end.total_price / 100;
                                console.log(response.discount_value);
                                var discounted = total - response.discount_value;
                                jQuery('.cart-subtotal .ajaxcart__price').text('$'+discounted);
                                jQuery('.cart-subtotal .ajaxcart__price').show();
                            });
                        },
                        error: function(err_response){
                            console.log(err_response);
                        }
                    });
                }else{
                    localStorage.setItem('discount_id',response.discount_name);
                    localStorage.setItem('rule_id',response.price_rule);
                    localStorage.setItem('discount_code',response.discount_code);
                    localStorage.setItem('discount_value', response.discount_value);
                    var ts = Math.round(new Date().getTime() / 1000);
                    var tsTomorrow = ts + (24 * 3600);
                    localStorage.setItem('discount_code_end',tsTomorrow);
                    jQuery.ajax({
                        url: "https://aromahigh.com/discount/"+response.discount_code
                    });

                    jQuery.getJSON('/cart.js', function(cart_end) {
                        console.log(cart_end);
                        var total = cart_end.total_price / 100;
                        console.log(response.discount_value);
                        var discounted = total - response.discount_value;
                        jQuery('.cart-subtotal .ajaxcart__price').text('$'+discounted);
                        jQuery('.cart-subtotal .ajaxcart__price').show();
                    });
                }
            },
            error: function(err_response){
                console.log(err_response);
            }
        });
    } );
}

var MGUtil={
    data:[],
    ini:0,
    total:0,
    action:'add',
    reset:function(){
        this.ini=0,this.total=0,this.data=[];
    },
    ajaxpost:function(uri,params,callback){
        $.ajax({
            type: 'POST',
            url : uri,
            dataType: 'json',
            async:true,
            data : params,
            success: function(){
                if(typeof callback === 'function'){
                    callback();
                }
            },
            error: function(error){
                console.log(error);
                if(error.responseJSON.description !== undefined){
                    alert(error.responseJSON.description);
                }
            }
        });
    },
    addItem:function(qty,id,properties,callback) {
        var params = {quantity:qty,id:id};
        if(properties != false){
            params.properties = properties;
        }
        MGUtil.ajaxpost('/cart/add.js',params,callback);
    },
    updateItem:function(qty,id,properties,callback){
        var params = {updates: {id: qty}};
        if(properties != false){
            params.properties = properties;
        }
        MGUtil.ajaxpost('/cart/update.js',params,callback);
    },
    recursive:function(){
        setTimeout(function(){
            $('.add-bundle-to-cart, .modal--is-active .add-to-cart, .add-to-cart--preorder, .add-to-cart-related, .fbt-add_to_cart, .fbt-fixed_add_to_cart').removeClass('btn--loading');
        }, 1200);

        var actionname = (MGUtil.action == 'update') ? 'updateItem' : 'addItem' ;
        //console.log(MGUtil.data[MGUtil.ini]);
        MGUtil[actionname](MGUtil.data[MGUtil.ini].qty,MGUtil.data[MGUtil.ini].id,MGUtil.data[MGUtil.ini].properties,function(){
            MGUtil.ini += 1;
            if(MGUtil.ini < MGUtil.total){
                MGUtil.recursive();
            }else{
                //arrow_cart_callback();
                MGUtil.reset();
                // console.log('all added');
                $('body').trigger('added.ajaxProduct');
                get_discount();
                return false;
            }
        });
    },
    begin:function(){
        /*SAMPLE*/
        /* SET YOUR ARRAY QTY's' ID's PROPERTIES(FALSE IF IS EMPTY)*/
        /* SET Action : 'add' , 'update' for performance */
        /* For delete use update with 0 for any item */
        MGUtil.data = [
            {"id":"12345","qty":2,"properties":{"data1":"1"}},
            {"id":"34567","qty":3,"properties":{"data2":"1"}},
            {"id":"67892","qty":1,"properties":{"data3":"1"}},
            {"id":"23456","qty":6,"properties":{"data4":"1"}}
        ];
        MGUtil.total  = MGUtil.data.length;
        MGUtil.action = 'add';
        MGUtil.recursive();
    }
}
//
//MGUtil.begin();
